export type Metadata = {
  categories: string[]
  year: string
}

export type People = {
  label: string
  value: string
}

export type Project = {
  slug?: string
  title?: string
  description?: string
  metadata?: Metadata
  images?: string[]
  content?: string
  featured_image?: string
  featured_background?: string
  featured_color?: string
  featured?: boolean
  external_link?: string
  people?: People[]
}