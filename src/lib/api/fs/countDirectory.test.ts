import countDirectory from './countDirectory'

test('count directory returns a number', () => {
  let count = countDirectory('src', 'content', 'projects')
  expect(count).toBeGreaterThan(0)
})