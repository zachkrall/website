import { readFileSync } from 'fs'
import { createPath, createMarkdown } from './utils'

export default (...p: string[]) => {
  const pagePath = createPath(...p)

  const realSlug = p.join('/').replace(p[0], '').replace(/\.md/g, '')
  const fileContents = readFileSync(pagePath, 'utf8')
  const { data, content } = createMarkdown(fileContents)

  return fileContents
    ? ({
        slug: realSlug,
        content,
        ...data,
      } as any)
    : ({} as any)
}
