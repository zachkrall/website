import { readdirSync } from "fs"
import { createPath } from './utils'
import getPage from './getPage'

export default (...p: string[]) => {
  const path = createPath(...p)
  const slugs = readdirSync(path)

  return slugs.length > 0 ? slugs.map(slug => getPage(...p, slug)) : []
}