import { readdirSync } from "fs"
import { createPath } from './utils'

export default (...p: string[]): number => {
  const path = createPath(...p)
  const slugs = readdirSync(path)

  return slugs.length || 0
}