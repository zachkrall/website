import { createPath } from './utils'

test('content path matches process cwd', () => {
  expect(createPath('src','content')).toBe(process.cwd() + '/src/content');
})

test('content/projects path matches process cwd', () => {
  expect(createPath('src','content', 'projects')).toBe(process.cwd() + '/src/content/projects');
})