import { join } from 'path'
import matter from "gray-matter"

export const createPath = (...args: string[]) => {
  return join(process.cwd(), ...args)
}

export const createMarkdown = (fileContents) => {
  /*
  if i wanted to include any other 
  markdown transformers, they would go here 
  */
  let { data, content } = matter(fileContents);
  return { data, content } as const;
}