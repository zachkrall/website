import { FC } from 'react'
import dynamic from 'next/dynamic'
import s from './SocialMedia.module.css'

const Data = [
  {
    url: 'https://twitter.com/zachkrall',
    label: 'Twitter',
    Icon: dynamic(
      () =>
        import(/* webpackPrefetch: true */ '../assets/svg/twitter.svg')
    ),
  },
  {
    url: 'https://are.na/zach-krall',
    label: 'Are.na',
    Icon: dynamic(
      () =>
        import(/* webpackPrefetch: true */ '../assets/svg/are.na.svg')
    ),
  },
  {
    url: 'https://instagram.com/zachkrall',
    label: 'Instagram',
    Icon: dynamic(
      () =>
        import(
          /* webpackPrefetch: true */ '../assets/svg/instagram.svg'
        )
    ),
  },
  {
    url: 'https://github.com/zachkrall',
    label: 'GitHub',
    Icon: dynamic(
      () =>
        import(/* webpackPrefetch: true */ '../assets/svg/github.svg')
    ),
  },
  {
    url: 'https://merveilles.town/@zachkrall',
    label: 'Merveilles',
    Icon: dynamic(
      () =>
        import(
          /* webpackPrefetch: true */ '../assets/svg/merveilles.svg'
        )
    ),
  },
  {
    url: 'https://app.rarible.com/zachkrall',
    label: 'Rarible',
    Icon: dynamic(
      () =>
        import(/* webpackPrefetch: true */ '../assets/svg/rarible.svg')
    ),
  },
  {
    url: 'https://workingnotworking.com/zachkrall',
    label: 'Working Not Working',
    Icon: dynamic(
      () =>
        import(
          /* webpackPrefetch: true */ '../assets/svg/workingnotworking.svg'
        )
    ),
  },
]

const SocialMedia: FC<any> = () => {
  return (
    <ul className={s.root}>
      {Data.map(({ url, label, Icon }) => {
        return (
          <li key={'social-media' + label + url}>
            <a href={url}>
              <Icon /> {label}
            </a>
          </li>
        )
      })}
    </ul>
  )
}

export default SocialMedia
