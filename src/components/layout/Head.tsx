import { FC } from 'react'
import NextHead from 'next/head'

const icons = [
  ['apple-touch-icon', '57x57', '/apple-icon-57x57.png'],
  ['apple-touch-icon', '60x60', '/apple-icon-60x60.png'],
  ['apple-touch-icon', '72x72', '/apple-icon-72x72.png'],
  ['apple-touch-icon', '76x76', '/apple-icon-76x76.png'],
  ['apple-touch-icon', '114x114', '/apple-icon-144x114.png'],
  ['apple-touch-icon', '120x120', '/apple-icon-120x120.png'],
  ['apple-touch-icon', '144x144', '/apple-icon-144x144.png'],
  ['apple-touch-icon', '152x152', '/apple-icon-152x152.png'],
  ['apple-touch-icon', '180x180', '/apple-icon-180x180.png'],
  ['icon', '192x192', '/android-icon-192x192.png'],
  ['icon', '96x96', '/android-icon-96x96.png'],
  ['icon', '32x32', '/android-icon-32x32.png'],
  ['icon', '16x16', '/android-icon-16x16.png'],
]

const Head: FC<{}> = ({ children }) => {
  return (
    <NextHead>
      {icons.map(([rel, sizes, href], index) => (
        <link
          rel={rel}
          type="image/png"
          sizes={sizes}
          href={href}
          key={index + rel + sizes}
        />
      ))}
      <link rel="manifest" href="/manifest.json" />
      <meta name="msapplication-TileColor" content="#ffffff" />
      <meta
        name="msapplication-TileImage"
        content="/ms-icon-144x144.png"
      />
      <meta name="theme-color" content="#ffffff" />
      {children}
    </NextHead>
  )
}

export default Head
