import Link from 'next/link'

import s from './Footer.module.css'

const Footer = () => {
  return (
    <footer className={s.root}>
      <ul className="inline font-medium">
        <li>
          <a className="link" href="https://buttondown.email/zachkrall">
            Newsletter
          </a>
        </li>
        <li>
          <a
            className="link"
            href="https://gitlab.com/zachkrall/website"
          >
            Source&nbsp;Code
          </a>
        </li>
        <li>
          <a className="link" href="/sitemap.xml">
            Sitemap
          </a>
        </li>
      </ul>
      <div className="mt-3">
        {
          /* Using a template literal string here allows for this
          to be treated as a single text leaf in the accessibility tree*/

          `© Copyright ${new Date().getFullYear()} Zachary Krall. All Rights
        Reserved.`
        }
      </div>
    </footer>
  )
}

export default Footer
