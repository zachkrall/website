import Link from 'next/link'
import styled from 'styled-components'

const N = styled.nav`
  font-weight: 500;
  padding: 1rem;

  a {
    text-decoration: none;
    color: inherit;
  }

  a:not(:last-child) {
    margin-right: 1em;
  }
`

const Nav = () => {
  return (
    <N>
      <Link href="/">
        <a>Zach Krall</a>
      </Link>
      <Link href="/">
        <a>Index</a>
      </Link>
      <Link href="/about">
        <a>About</a>
      </Link>
    </N>
  )
}

export default Nav
