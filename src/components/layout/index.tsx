import { useState, FC, useEffect } from 'react'
import Head from './Head'
import Nav from './Nav'
import Footer from './Footer'

interface LayoutProps {
  title?: string
}

const Layout: FC<LayoutProps> = ({ title, children }) => {
  let [isMobile, setIsMobile] = useState(true)

  useEffect(() => {
    let agents = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
    setIsMobile(agents.test(navigator.userAgent))
  }, [])

  title = title ? title + ' • Zach Krall' : 'Zach Krall'

  return (
    <div>
      <Head>
        <title>{title}</title>
      </Head>

      <Nav />
      <main>{children}</main>
      <Footer />
      {/* <Orb /> */}
    </div>
  )
}

export default Layout
