import { forwardRef, useEffect, useRef, useCallback } from 'react'
import { Canvas, useThree, useFrame } from 'react-three-fiber'

function Camera(props) {
  const ref = useRef<any>()
  const { setDefaultCamera, camera } = useThree()

  useEffect(() => {
    setDefaultCamera(ref.current)
  }, [])

  const updateFOV = useCallback(() => {
    if (ref.current) {
      ref.current.fov =
        2 * Math.atan(window.innerHeight / 2 / 600) * (180 / Math.PI)
      ref.current.position.x = 0
      ref.current.position.y = 0
      ref.current.position.z = 600
    }
  }, [ref])

  useEffect(() => {
    updateFOV()

    window.addEventListener('resize', updateFOV)
    return () => window.removeEventListener('reize', updateFOV)
  }, [ref])

  useFrame(() => {
    return ref.current?.updateMatrixWorld()
  })

  return <perspectiveCamera ref={ref} {...props} />
}

export default ({ children }) => {
  return (
    <Canvas
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
        height: '100vh',
        zIndex: -1,
      }}
    >
      <Camera />
      {children}
    </Canvas>
  )
}
