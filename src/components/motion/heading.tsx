import styled, { keyframes } from 'styled-components'

const Animation = keyframes`
  from {
    transform: translate(0, 100%);
  }

  to {
    transform: translate(0, 0);
  }
`

const TextWrapper = styled.span`
  font-size: 2rem;
  font-weight: 500;

  position: relative;
  overflow: hidden;

  display: inline-block;
`
const TextNode = styled.span`
  position: relative;
  top: 0;
  left: 0;

  animation: ${Animation};
  animation-duration: ${({ duration: x }) => x + 's'};
`

const Heading = ({ text }) => {
  return (
    <div>
      {text
        .replace(/ /g, '\u00A0')
        .split('')
        .map((t, index, arr) => (
          <TextWrapper key={t + index}>
            <TextNode duration={(index / arr.length) * 40}>
              {t}
            </TextNode>
          </TextWrapper>
        ))}
    </div>
  )
}

export default Heading
