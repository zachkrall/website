module.exports = {
  title: 'Zach Krall',
  titleTemplate: '%s - Zach Krall',
  description:
    'Zach Krall is a developer and designer in New York City. He holds an MFA in Design + Technology from Parsons School of Design.',
  openGraph: {
    type: 'website',
    locale: 'en_US',
    url: 'https://zachkrall.com',
    site_name: 'Zach Krall',
  },
  twitter: {
    handle: '@zachkrall',
    site: '@zachkrall',
    cardType: 'summary_large_image',
  },
}
