---
title: See You in Meatspace
description:
metadata:
  categories:
    - Art
  year: 2015
  tags: photo sculpture
images:
  - "/media/see-you-in-meatspace_zach-krall_01.jpg"
  - "/media/see-you-in-meatspace_zach-krall_02.jpg"
  - "/media/see-you-in-meatspace_zach-krall_03.jpg"
  - "/media/see-you-in-meatspace_zach-krall_04.jpg"
  - "/media/see-you-in-meatspace_zach-krall_05.jpg"
  - "/media/see-you-in-meatspace_zach-krall_06.jpg"
---

Meatspace is a way of positioning physical space as outside of cyberspace as a primary existence. Each portrait is physically demanding in it's production with images printed through an inkjet process onto Latex sheets. The image is then physically manipulated (either by stretching, twisting, or other means) and stapled onto canvas stretchers. In some cases the restrictions of the physical world create tears and breaks within the image, revealing limits that do not exist in cyberspace.

![See You in Meatspace](/media/see-you-in-meatspace_zach-krall_01.jpg)

![See You in Meatspace](/media/see-you-in-meatspace_zach-krall_02.jpg)

![See You in Meatspace](/media/see-you-in-meatspace_zach-krall_03.jpg)

![See You in Meatspace](/media/see-you-in-meatspace_zach-krall_04.jpg)

![See You in Meatspace](/media/see-you-in-meatspace_zach-krall_05.jpg)

![See You in Meatspace](/media/see-you-in-meatspace_zach-krall_06.jpg)
