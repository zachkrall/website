---
title: Algorithm Adjustment
year: 2020
category: Art
images:
  - "/media/algorithm-adjustment_zach-krall_01.jpg"
  - "/media/algorithm-adjustment_zach-krall_02.jpg"
  - "/media/algorithm-adjustment_zach-krall_03.jpg"
  - "/media/algorithm-adjustment_zach-krall_04.jpg"
  - "/media/algorithm-adjustment_zach-krall_05.jpg"
description: |
  Here's a description
---

Collaboration with Amber Hurwitz, Kathryn McCawly, Timmy Ong, Sherry Shao, and Sofia Von Hauska Valtierra.

![](/media/algorithm-adjustment_zach-krall_01.jpg)

![](/media/algorithm-adjustment_zach-krall_02.jpg)

![](/media/algorithm-adjustment_zach-krall_03.jpg)

![](/media/algorithm-adjustment_zach-krall_04.jpg)

![](/media/algorithm-adjustment_zach-krall_05.jpg)
