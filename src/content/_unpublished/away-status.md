---
title: Away Status
description: Site-specific video projection
metadata:
  year: 2015
  categories:
    - Art
    - Installation
  medium: Site-specific Video Projection (color, silent)
  dimensions: Variable
images:
  - "/media/away-status_zach-krall_01.jpg"
  - "/media/away-status_zach-krall_02.jpg"
  - "/media/away-status_zach-krall_video.png"
---

![Away Status](/media/away-status_zach-krall_01.jpg)

![Away Status](/media/away-status_zach-krall_02.jpg)

<div class="videoWrapper"><iframe src="https://player.vimeo.com/video/125755378?color=ffffff&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
