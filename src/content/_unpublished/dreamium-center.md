---
title: Dreamium Center
year: 2020
category: Design, Code
images:
  - "/media/IMG_2196.jpg"
  - "/media/IMG_2227.jpg"
  - "/media/IMG_2224.jpg"
---

Dreamium Center is a fictional luxury dream spa, where premium dreams are crafted by celebrity dream architects.

![](/media/IMG_2196.jpg)

To experience the installation, participants would check into their appointment using their DreamPass which would play a surreal soundscape. While listening to their soundscape, participants were invited to explore a sensory room which included an infinite loop projection expanding the space into a surreal video collage.

![](/media/IMG_2227.jpg)

Collaboration with Amber Hurwitz, Jordan McRae, Yunfei Song, Xiaosu Yu

![](/media/IMG_2224.jpg)
