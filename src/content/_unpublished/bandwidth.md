---
title: Bandwidth (Unique Forms of Continuity)
metadata:
  year: 2016
  categories:
    - Art
    - Installation
images:
  - "/media/bandwidth.jpg"
---

![Bandwidth (Unique Forms of Continuity)](/media/bandwidth.jpg)
