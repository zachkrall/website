---
title: Chroma Cleanse 2.0
description:
metadata:
  year: 2015
  categories:
    - Art
    - Video
  medium: Video (color, sound)
  duration: 1 min 59 sec
  project_status: Completed
images:
  - "/media/chroma-cleanse_zach-krall_video.jpg"
---

<div class="videoWrapper"><iframe src="https://player.vimeo.com/video/122816575?color=8CAD56&title=0&byline=0&portrait=0" width="711" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
