---
title: Slime Futures
description:
metadata:
  year: 2019
  categories:
    - Art
  tools:
    - Raspberry Pi
    - VLC
    - Bash
  materials: |
    Cast Acrylic Sheeting, Silicone, Latex, Glue, Acrylic Paint, Shaving Cream, Baking Soda, Gel Soap, LCD Display, Microcontroller, Video (color, sound)
images:
  - "/media/IMG_2117.jpg"
  - "/media/IMG_2237.jpg"
  - "/media/IMG_2044.jpg"
  - "/media/IMG_2062.jpg"
textColor: "#BBE6C1"
bgColor: "#39644A"
---

![Slime Futures](/media/IMG_2117.jpg)

The work aims to investigate how computer models assess and understand an individual.

![Slime Futures](/media/IMG_2237.jpg)

Since future predictions are created out of data from the past, identity becomes a feedback loop.

![Slime Futures](/media/IMG_2044.jpg)

This cancellation of time creates a hauntological condition by employing the past as a means to move towards the future.

![Slime Futures](/media/IMG_2062.jpg)

If it were possible to view this data, accumulated overtime and visible from multiple vantage points, what would it look like?
