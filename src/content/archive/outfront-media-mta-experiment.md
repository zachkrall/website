---
title: Outfront Media / MTA Map Experiments
description:
metadata:
  year: 2019
  categories:
    - Design
    - Code
    - Generative
  tools:
    - Webpack
    - JavaScript
    - p5.js
people:
  - label: "Client"
    value: "Outfront Media"
images:
  - "/media/outfront-media_zach-krall_IMG_0778.jpg"
  - "/media/Sheepshead-Bay.png"
  - "/media/14th-st.png"
  - "/media/67th-ave.png"
  - "/media/96th-st.png"
  - "/media/Bay-Pky.png"
---

![Outfront Media / MTA Map Experiments](/media/outfront-media_zach-krall_IMG_0778.jpg)

Visual experiment of a dynamic MTA subway map. Using [Outfront Media's Live Boards](https://www.outfrontmedia.com/media/digital/liveboards), the application uses p5.js to illustrate hotspots in the transit system.

Creating drawings using GeoJSON of all 472 NYC subway stations.

![Outfront Media / MTA Map Experiments](/media/mta_drawings.png)

What would it look like to plot heat maps of activity within the subway system?

![Outfront Media / MTA Map Experiments](/media/mta_slide_2.png)
