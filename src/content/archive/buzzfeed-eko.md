---
featured: false
featured_image: '/media/buzzfeed.png'
featured_color: '#444'
title: BuzzFeed by eko
description: |
  Interactive video
people:
  - label: Client
    value: BuzzFeed
metadata:
  year: 2020
  categories:
    - Code
    - Interactive
    - Web
  tools:
    - Eko Studio
    - JavaScript
    - React
images:
  - /media/buzzfeed.png
---

![BuzzFeed Eko](/media/buzzfeed.png)

Creating interactive video content with BuzzFeed.
