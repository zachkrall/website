---
title: Pim Ketras
description: |
  Machine Learning Model using Kim Petras's Lyrics
external_link: https://pim-ketras.glitch.me
metadata:
  year: 2020
  categories:
    - Code
    - Machine Learning
  tools:
    - JavaScript
    - React
images:
  - "/media/pim-ketras.png"
---

![](/media/pim-ketras.png)
