---
title: Inside Cyber Command
description: |
  Sculpture
metadata:
  year: 2013
  categories:
    - Art
    - Installation
images:
  - "/media/inside-cyber-command_zach-krall_01.jpg"
  - "/media/inside-cyber-command_zach-krall_02.jpg"
  - "/media/inside-cyber-command_zach-krall_03.jpg"
  - "/media/inside-cyber-command_zach-krall_04.jpg"
---

Archival Inkjet Print, Plexiglas, Fluorescent Lighting, Found Computer Units.

[DIS Magazine](http://dismagazine.com/discussion/73272/benjamin-bratton-machine-vision/)

![Inside Cyber Command](/media/inside-cyber-command_zach-krall_01.jpg)

![Inside Cyber Command](/media/inside-cyber-command_zach-krall_02.jpg)

![Inside Cyber Command](/media/inside-cyber-command_zach-krall_03.jpg)

![Inside Cyber Command](/media/inside-cyber-command_zach-krall_04.jpg)
