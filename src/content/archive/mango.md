---
featured: true
featured_color: "#4AABE4"
featured_image: "/media/mango_zach-krall_01.jpg"
featured_background: "/media/mango_background.jpg"
title: Leandra Medine x MANGO
description: |
  React.js slot machine for retail experience.
metadata:
  year: 2019
  categories:
    - Code
    - Interactive
  tools:
    - JavaScript
    - Webpack
    - React
    - Node.js
    - Express.js
    - Electron
  typefaces:
    - "<a href='https://fonts.google.com/specimen/Roboto'>Roboto</a>"
people:
  - label: "Design"
    value: "DE–YAN"
  - label: "Client"
    value: "MANGO"
images:
  - "/media/mango_zach-krall_01.jpg"
  - "/media/mango_zach-krall_02.jpg"
  - "/media/mango_zach-krall_electron.png"
---

![Leandra Medine x MANGO Slot Machine](/media/mango_zach-krall_01.jpg)

As part of an event experience designed by DE–YAN. I implemented a custom slot machine application using React and Electron.

<div class="videoWrapper" style="padding-bottom:70%;"><video autoplay muted loop playsinline><source src="/videos/mango_zach-krall_electron.mp4" type="video/mp4"/></video></div>

![Leandra Medine x MANGO Slot Machine](/media/mango_zach-krall_02.jpg)
