---
title: p5snap
metadata:
  year: 2020
  categories: 
    - Code
    - Open Source
description: |
  p5snap is a command-line interface that creates and saves snapshots of a p5 sketch.
images:
  - "/media/p5snap_example.png"
---

p5snap is a command-line interface that creates and saves snapshots of a p5 sketch.

<a class="dib ba bw--thin b--black br2 near-black link ph2 pv1 hover-bg-near-black hover-white" href="https://github.com/zachkrall/p5snap">GitHub</a> <a class="dib ba bw--thin b--black br2 ml3 near-black link ph2 pv1 hover-bg-near-black hover-white" href="https://npmjs.com/package/p5snap">NPM</a>

## installation

with `npm`:

```shell
npm -g install p5snap
```

## usage

to start p5snap, provide a relative file path and the number of images that should be saved

```shell
p5snap <FILE-PATH> -n <NUMBER-OF-IMAGES>
```

for example:

```shell
p5snap ./mySketch.js -n 20
```

will create:<br/>
• mySketch_0.png<br/>
• mySketch_1.png<br/>
• mySketch_2.png<br/>
• ...<br/>
• mySketch_19.png

![](/media/p5snap_example.png)

### as a module

you can bring p5snap into your existing node.js build tools by importing the snap module.

for example:

```
const path = require('path')
const snap = require('p5snap/modules/snap.js')

snap({
  sketch_path: path.resolve(__dirname, './mySketch.js'),
  output_path: path.resolve(__dirname),
  width: 1920,
  height: 1080,
  instance: false,
  filename: 'mySketch',
})
```

### instance mode

if your sketch is written as a p5 instance, you can use the `--instance` flag to execute **p5snap** in instance mode

instance mode does not require `new p5()`

## limitations

**p5snap** currently only saves a single `<canvas/>` context. If your p5 drawing uses or draws DOM elements, it will not be included in the image.

## contributing

Contributions, issues and feature requests are welcome.<br/>Feel free to check [issues](https://github.com/zachkrall/p5snap/issues/) page if you want to contribute.

## license

Copyright © 2020 [Zach Krall](https://zachkrall.com).<br/>This project is [MIT](https://github.com/zachkrall/p5snap/blob/master/LICENSE) licensed.
