---
title: Data Visualization Keynotes
description: |
  Promotional graphics for MS Data Visualization department
metadata:
  year: 2020
  categories:
    - Design
    - Code
    - Generative
    - 3D
  tools:
    - JavaScript
    - p5.js
    - Vue
  typefaces:
    - "<a href='https://www.newschool.edu/marketing-communication/diy/'>Neue</a>"
people:
  - label: "Client"
    value: "Parsons School of Design"
    url: "https://parsons.edu"
images:
  - "/media/parsons-msdv-keynotes_zach-krall_video_00.png"
  - "/media/parsons-msdv-keynotes_zach-krall_video_01.png"
  - "/media/parsons-msdv-keynotes_zach-krall_video_03.png"
---

<div class="videoWrapper"><video autoplay muted loop playsinline><source src="/videos/msdv-keynotes_zach-krall_v03.mp4" type="video/mp4"/></video></div>

Promotional design for the MS Data Visualization department's end of year keynotes at Parsons School of Design.

Motion graphics were used throughout the event's livestream.

<div class="videoWrapper"><iframe id="ls_embed_1592865097" src="https://livestream.com/accounts/1369487/events/9101448/player?width=640&height=360&enableInfoAndActivity=true&defaultDrawer=&autoPlay=true&mute=false" width="640" height="360" frameborder="0" scrolling="no" allowfullscreen> </iframe></div>

Early design experiments used marquee wrapped geometries.

<div class="videoWrapper"><video autoplay muted loop playsinline><source src="/videos/msdv-keynotes_zach-krall_v00.mp4" type="video/mp4"/></video></div>

<div class="videoWrapper"><video autoplay muted loop playsinline><source src="/videos/msdv-keynotes_zach-krall_v01.mp4" type="video/mp4"/></video></div>
