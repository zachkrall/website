---
featured: false
featured_image: "/media/phase-mask_zach-krall_cover.png"
featured_color: "#eee"
external_link: https://phasemask.zachkrall.com
title: Phase Mask
description: |
  Live-coding environment
metadata:
  year: 2020
  categories:
    - Design
    - Code
    - 3D
  tools:
    - JavaScript
    - Tensorflow.js
    - CodeMirror
    - Three.js
    - Vue
images:
  - "/media/phase-mask_zach-krall_cover.png"
  - "/media/phase-mask_zach-krall_00.png"
---

![Phase Mask](/media/phase-mask_zach-krall_cover.png)

<div class="videoWrapper"><video autoplay muted loop playsinline><source src="/videos/phase-mask_zach-krall_00.mp4" type="video/mp4"/></video></div>
