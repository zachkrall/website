---
title: Glimpse
description: |
  Website for open-source image editor Glimpse.
external_link: https://glimpse-editor.github.io
metadata:
  categories:
    - Code
    - Web
  year: 2020
  typefaces:
    - <a href="https://manropefont.com/" rel="noopener noreferrer" target="_blank">Manrope</a>
  tools:
    - Bootstrap
    - Hugo
images:
  - "/media/glimpse_00.png"
---

![](/media/glimpse_00.png)
