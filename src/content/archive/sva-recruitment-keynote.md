---
title: SVA Recruitment Presentation
description:
metadata:
  year: 2018
  categories:
    - Design
  tools:
    - Photoshop
    - Premiere
    - After Effects
    - Keynote
  typefaces:
    - Trade Gothic
    - Monday
people:
  - label: "Client"
    value: "School of Visual Arts"
images:
  - "/media/sva-recruitment_01.png"
---

2018-2019 Academic Year

![](/media/sva-recruitment_01.png)

2017-2018 Academic Year

![](/media/sva-recruitment_02.png)

2016-2017 Academic Year

![](/media/sva-recruitment_03.png)
