---
featured: true
featured_image: "/media/schrock_sq.png"
featured_color: "#ccc"
title: Roddy Schrock
external_link: https://roddyschrock.net
description: |
  Personal website for Roddy Shrock
metadata:
  categories:
    - Code
    - Web
  year: 2020
  typefaces:
    - <a href="https://klim.co.nz/retail-fonts/financier-display/" rel="noopener noreferrer" target="_blank">Financier Display</a>
  tools:
    - Vue
    - Kirby
    - WebGL
people:
  - label: "Client"
    value: "Roddy Schrock"
  - label: "Design and Concept"
    value: "Richard The"
    url: "https://richardthe.com"
images:
  - "/media/schrock_1.png"
  - "/media/schrock_demo.png"
  - "/media/schrock_2.png"
  - "/media/schrock_3.png"
  - "/media/schrock_4.png"
---

<div class="videoWrapper"><video autoplay muted loop playsinline><source src="/videos/schrock_01.mp4" type="video/mp4"/></video></div>

Personal website for Roddy Schrock. The site content is displayed with Vue.js and Kirby CMS.

Text color is animated by placing a WebGL shader ontop of the website container.

<div class="videoWrapper"><iframe width="400px" height="300px;" src="/widgets/roddy-schrock/index.html" style="border:0;" scrolling="no"></iframe></div>

<div class="videoWrapper"><video autoplay muted loop playsinline><source src="/videos/schrock_02.mp4" type="video/mp4"/></video></div>
