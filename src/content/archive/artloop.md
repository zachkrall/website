---
title: artloop
metadata:
  year: 2019
  categories:
    - Code
    - Open Source
description: |
  artloop is an Electron app that cycles through a folder containing web based projects.
images:
  - "/media/artloop_zach-krall_01.png"
  - "/media/artloop_zach-krall_02.jpg"
---

artloop is an Electron app that cycles through a folder containing web based projects.

<a class="dib ba bw--thin b--black br2 near-black link ph2 pv1 hover-bg-near-black hover-white" href="https://github.com/zachkrall/artloop">GitHub</a> <a class="dib ba bw--thin b--black br2 near-black link ph2 pv1 hover-bg-near-black hover-white ml3" href="https://artloop.now.sh">artloop.now.sh</a>

![](/media/artloop_zach-krall_01.png)

artloop was experimentally installed as a digital display at Parsons School of Design in the Design + Technology department. A Raspberry Pi connected to an HD display at Parsons would automatically start artloop once it was turned on.

![](/media/artloop_zach-krall_02.jpg)
