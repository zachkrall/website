---
featured: true
featured_color: "#0071DC"
featured_image: "/media/camp-by-walmart_featured.png"
title: Camp by Walmart Activity
description: |
  Interactive Video
people:
  - label: Client
    value: Walmart
external_link: https://www.campbywalmart.com
metadata:
  year: 2020
  typefaces: 
    - <a href="https://one.walmart.com/content/people-experience/associate-brand-center.html" target="_blank" rel="noopener noreferrer">Bogle</a>
  categories:
    - Code
    - Interactive
    - Web
  tools:
    - Eko Studio
    - JavaScript
    - React
  links:
    - label: Walmart Press Release
      url: https://corporate.walmart.com/newsroom/2020/07/01/walmart-launches-virtual-summer-camp-and-drive-in-movie-theater-to-help-families-make-the-most-of-summer
images:
  - /media/camp-by-walmart_01.png
  - /media/camp-by-walmart.png
---

![](/media/camp-by-walmart_01.png)

Implemented interactive React components in original video content for Camp by 
Walmart Activity, a virtual summer camp housed within the Walmart iOS and
Android applications.

![](/media/camp-by-walmart_02.png)

Inside the application, an interactive original series ca nbe selected from the 
*what's new* menu.

![](/media/camp-by-walmart.png)

The video node logic for each episode was built out using eko's proprietary 
video editor using React.js to manage the game play state and components used 
for on-screen decision actions.

![](/media/camp-by-walmart_03.png)
