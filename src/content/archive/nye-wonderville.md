---
title: NYE Wonderville Algorave
description: |
metadata:
  year: 2019
  categories:
    - Algorave
images:
  - '/media/wonderville-nye-algorave-2019_zach-krall_maxwell-neely-cohen_01.png'
  - '/media/wonderville-nye-algorave-2019_zach-krall_maxwell-neely-cohen_02.png'
  - '/media/wonderville-nye-algorave-2019_zach-krall_maxwell-neely-cohen_03.png'
  - '/media/wonderville-nye-algorave-2019_zach-krall_maxwell-neely-cohen_04.png'
  - '/media/wonderville-nye-algorave-2019_zach-krall_maxwell-neely-cohen_05.png'
---

<div class="video_16-9">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5pTFSMckVtk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
