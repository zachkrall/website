---
title: The Oxbow School Recruitment Keynote
description:
metadata:
  year: 2018
  categories:
    - Design
  tools:
    - Photoshop
    - Keynote
  typefaces:
    - ITC Franklin Gothic
people:
  - label: "Client"
    value: "The Oxbow School"
images:
  - "/media/oxbow_01.png"
---

![](/media/oxbow_01.png)

![](/media/oxbow_02.png)

![](/media/oxbow_03.png)

![](/media/oxbow_04.png)

![](/media/oxbow_05.png)
