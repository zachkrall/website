---
title: Instagram Face Filters
description:
metadata:
  year: 2019
  categories:
    - Design
    - Code
    - 3D
    - Interactive
  tools:
    - SparkAR
    - Photoshop
images:
  - "/media/instagram_face-filter_zach-krall_math-still.png"
  - "/media/instagram_face-filter_zach-krall_sparkle-still.png"
---

Face filters created in 2019 as part of the Instagram Developers Beta Program.

<div class="videoWrapper"><video autoPlay muted loop playsInline><source src="/videos/instagram_face-filter_zach-krall_math.mp4" type="video/mp4"/></video></div>

<div class="videoWrapper"><video autoPlay muted loop playsInline><source src="/videos/instagram_face-filter_zach-krall_sparkle.mp4" type="video/mp4"/></video></div>
