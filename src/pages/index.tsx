import React from 'react'
import { GetStaticProps } from 'next'
import Link from 'next/link'
import Image from 'next/image'
import { getDirectory } from '@lib/api'
import Layout from '@components/layout'

import Heading from '@components/motion/heading'

export default function IndexPage({ items }) {
  return (
    <>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: 'repeat(3, 1fr)',
          padding: '1em',
          gridGap: '5em',
        }}
      >
        {items
          .sort((a, b) => b.metadata.year - a.metadata.year)
          .map((i, key) => {
            let w = 500
            let h = (w * 9) / 16
            return (
              <Link
                as={i.path}
                href="/archive/[slug]"
                key={i.title + key}
              >
                <a
                  style={{
                    margin: '-1px 0 0 -1px',
                    color: 'black',
                    textDecoration: 'none',
                  }}
                >
                  <Image
                    src={i.images[0]}
                    width={w}
                    height={h}
                    objectFit="cover"
                    objectPosition="center"
                  />
                  <div style={{ fontWeight: 500 }}>{i.title}</div>
                  <div style={{ opacity: 0.5 }}>{i.description}</div>
                </a>
              </Link>
            )
          })}
      </div>
    </>
  )
}

IndexPage.Layout = Layout

export const getStaticProps: GetStaticProps = async (/* context */) => {
  const items = getDirectory('src', 'content', 'archive')

  return {
    props: {
      items: items.map((posts: any) => ({
        ...posts,
        path: posts.slug.replace(/\/content/g, ''),
      })),
    },
  }
}
