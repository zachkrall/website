import type { NextApiRequest, NextApiResponse } from 'next'
import { createCanvas, loadImage, registerFont } from 'canvas'
import path from 'path'
import { getPage } from '../../lib/api'

function createPath(p) {
  return path.resolve('./public', p)
}

function createImage(text?, img?) {
  let canvas,
    fontPath = createPath('../src/assets/fonts/NeueMontreal-Medium.otf')

  registerFont(fontPath, { family: 'Neue Montreal' })

  let width = 1200,
    height = 630
  canvas = createCanvas(width, height)
  let ctx = canvas.getContext('2d')
  ctx.fillStyle = '#000'
  ctx.fillRect(0, 0, width, height)

  let fontSize = 48

  if (img && text) {
    ctx.fillStyle = '#fff'
    ctx.fillRect(width * 0.5, 0, width * 0.5, height)
    drawImageProp(ctx, img, width * 0.5, 0, width * 0.5, height)

    ctx.font = `${fontSize}px Neue Montreal`
    ctx.fillStyle = '#FFFFFF'
    ctx.textBaseline = 'top'
    wrapText(ctx, text, 30, 30, width * 0.5 - 60, fontSize)
    ctx.fillStyle = '#999999'
    ctx.textBaseline = 'bottom'
    ctx.fillText('Zach Krall', 30, height - 30)
  } else if (!img && text) {
    ctx.font = `${fontSize}px Neue Montreal`
    ctx.fillStyle = '#FFFFFF'
    ctx.fillText(
      text,
      width * 0.5 - ctx.measureText(text).width * 0.5,
      height * 0.5 - fontSize * 0.5 - 5
    )
    ctx.fillStyle = '#999999'
    ctx.fillText(
      'Zach Krall',
      width * 0.5 - ctx.measureText('Zach Krall').width * 0.5,
      height * 0.5 + fontSize * 0.5 + 5
    )
  } else {
    ctx.font = `${fontSize}px Neue Montreal`
    ctx.fillStyle = '#FFFFFF'
    ctx.fillText(
      'Zach Krall',
      width * 0.5 - ctx.measureText('Zach Krall').width * 0.5,
      height * 0.5
    )
  }

  return canvas.toBuffer('image/png')
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  let data,
    slug = req.query['s'] || 'Hello, World!'

  try {
    let page = getPage('src', 'content', 'archive', slug + '.md')
    let text = page.title

    if (page.images[0]) {
      let img = await loadImage(createPath(page.images[0]))
      data = createImage(text, img)
    } else {
      data = createImage(text)
    }
  } catch (e) {
    data = createImage()
  } finally {
    res.setHeader('Content-Type', 'image/png')
    res.send(data)
  }
}

/**
 * By Ken Fyrstenberg Nilsen
 *
 * drawImageProp(context, image [, x, y, width, height [,offsetX, offsetY]])
 *
 * If image and context are only arguments rectangle will equal canvas
 */
function drawImageProp(ctx, img, x, y, w, h, offsetX?, offsetY?) {
  if (arguments.length === 2) {
    x = y = 0
    w = ctx.canvas.width
    h = ctx.canvas.height
  }

  // default offset is center
  offsetX = typeof offsetX === 'number' ? offsetX : 0.5
  offsetY = typeof offsetY === 'number' ? offsetY : 0.5

  // keep bounds [0.0, 1.0]
  if (offsetX < 0) offsetX = 0
  if (offsetY < 0) offsetY = 0
  if (offsetX > 1) offsetX = 1
  if (offsetY > 1) offsetY = 1

  var iw = img.width,
    ih = img.height,
    r = Math.min(w / iw, h / ih),
    nw = iw * r, // new prop. width
    nh = ih * r, // new prop. height
    cx,
    cy,
    cw,
    ch,
    ar = 1

  // decide which gap to fill
  if (nw < w) ar = w / nw
  if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh // updated
  nw *= ar
  nh *= ar

  // calc source rectangle
  cw = iw / (nw / w)
  ch = ih / (nh / h)

  cx = (iw - cw) * offsetX
  cy = (ih - ch) * offsetY

  // make sure source rectangle is valid
  if (cx < 0) cx = 0
  if (cy < 0) cy = 0
  if (cw > iw) cw = iw
  if (ch > ih) ch = ih

  // fill image in dest. rectangle
  ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h)
}

function wrapText(context, text, x, y, maxWidth, lineHeight) {
  var words = text.split(' ')
  var line = ''

  for (var n = 0; n < words.length; n++) {
    var testLine = line + words[n] + ' '
    var metrics = context.measureText(testLine)
    var testWidth = metrics.width
    if (testWidth > maxWidth && n > 0) {
      context.fillText(line, x, y)
      line = words[n] + ' '
      y += lineHeight
    } else {
      line = testLine
    }
  }
  context.fillText(line, x, y)
}
