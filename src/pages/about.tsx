import { Fragment } from 'react'
import Layout from '@components/layout'
import SocialMedia from '@components/SocialMedia'

import CV from '@config/cv.json'

const AboutPage = () => {
  return (
    <>
      {/* <div className="p-4">
        <SocialMedia />
      </div>
      <div className="p-4">
        <h1 className="text-xl">CV</h1>
        <div>
          {Object.entries(CV).map(([section, obj], key) => (
            <Fragment key={section + key}>
              <h1>{section}</h1>
              <div className="">
                {Object.entries(obj)
                  .sort(([a, x], [b, y]) => Number(b) - Number(a))
                  .map(([year, list]) => (
                    <>
                      <div className="inline-block w-20">{year}</div>
                      <div className="inline-block max-w-lg align-top">
                        {list.map((i) => (
                          <div
                            dangerouslySetInnerHTML={{ __html: i }}
                          ></div>
                        ))}
                      </div>
                      <div>&nbsp;</div>
                    </>
                  ))}
              </div>
            </Fragment>
          ))}
        </div>
      </div> */}

      <p style={{ padding: '1rem', maxWidth: '30em' }}>
        Zach Krall is a developer and designer in New York City. He
        holds an MFA in Design + Technology from Parsons School of
        Design.
      </p>
    </>
  )
}

AboutPage.Layout = Layout

export default AboutPage
