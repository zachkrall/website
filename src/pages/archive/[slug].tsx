import { useRouter } from 'next/router'
import Link from 'next/link'
import Head from 'next/head'
import ErrorPage from 'next/error'

import styled from 'styled-components'

import { getPage, getDirectory } from '@lib/api'

import Markdown from 'markdown-to-jsx'
import Layout from '@components/layout'

const Wrapper = styled.div`
  p {
    padding: 1rem;
    max-width: 30em;
    font-weight: 500;
  }

  .videoWrapper {
    position: relative;
    top: 0;
    left: 0;
    height: 0px;
    padding-bottom: 56.25%;
  }
  .videoWrapper video,
  .videoWrapper iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`

function Post({ post, slug }) {
  const router = useRouter()

  if (!router.isFallback && !post?.slug) {
    return <ErrorPage statusCode={404} />
  }

  return (
    <>
      <Head>
        <title>Zach Krall</title>
        <meta name="description" content="" />

        <meta
          property="og:url"
          content={`https://zachkrall.com/archive/${slug}`}
        />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Zach Krall" />
        <meta property="og:description" content="" />
        <meta
          property="og:image"
          content={`https://zachkrall.com/api/opengraph.png?s=${slug}`}
        />

        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:domain" content="zachkrall.com" />
        <meta
          property="twitter:url"
          content="http://zachkrall.com/archive/buzzfeed-eko"
        />
        <meta name="twitter:title" content="Zach Krall" />
        <meta name="twitter:description" content="" />
        <meta
          name="twitter:image"
          content={`https://zachkrall.com/api/opengraph.png?s=${slug}`}
        />
      </Head>
      {router.isFallback ? (
        <div>Loading…</div>
      ) : (
        <Wrapper>
          <div>
            <h2 style={{ padding: '1rem' }}>
              <span>{post.title}</span>
            </h2>
            <Markdown
              options={{
                overrides: {
                  p: (props) => {
                    return props.children.some(
                      (child) => typeof child === 'string'
                    ) ? (
                      <p {...props} />
                    ) : (
                      <div {...props} />
                    )
                  },
                },
              }}
            >
              {post.content}
            </Markdown>
          </div>
        </Wrapper>
      )}
    </>
  )
}

Post.Layout = Layout

export default Post

export async function getStaticProps({ params }) {
  const post = getPage(
    'src',
    'content',
    'archive',
    String(params.slug + '.md')
  )

  return {
    props: {
      post,
      slug: params.slug,
    },
  }
}

export async function getStaticPaths() {
  const posts = getDirectory('src', 'content', 'archive').map(
    (post: any) => ({
      params: {
        slug: post.slug.replace('/content/archive/', ''),
      },
    })
  )

  return {
    paths: posts,
    fallback: false,
  }
}
