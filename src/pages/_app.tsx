import { FC, useEffect, useRef, useState, useCallback } from 'react'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import imagesloaded from 'imagesloaded'
import { useElementScroll } from 'framer-motion'
import { useThree } from 'react-three-fiber'
import WebGL from '@components/WebGL'

// tailwind
import '@assets/css/global.css'

// const ScrollContainer = styled.div`
//   position: fixed;
//   top: 0;
//   left: 0;
//   height: 100vh;
//   width: 100vw;
//   overflow: auto;
// `

export default function App({ Component, pageProps }) {
  const router = useRouter()

  const Nothing = ({ children }) => <>{children}</>
  const Wrapper = Component.Layout || Nothing

  // const [images, setImages] = useState<any[]>()
  const scrollContainer = useRef<any>()
  // const { scrollY } = useElementScroll(scrollContainer)
  // const imageRefs = useRef<any>([])

  // const updateImagePositions = useCallback(
  //   (currentScroll) => {
  //     imageRefs.current.map((img, index) => {
  //       if (images && images[index]) {
  //         let { top, left, width, height } = images[index]

  //         let y =
  //           currentScroll - top + window.innerHeight / 2 - height / 2
  //         let x = left - window.innerWidth / 2 + width / 2

  //         img.position.x = x
  //         img.position.y = y
  //       }
  //     })
  //   },
  //   [images, imageRefs]
  // )

  // useEffect(() => {
  //   if (scrollContainer.current) {
  //     let arr: any[] = [
  //       ...scrollContainer.current.querySelectorAll('img'),
  //     ]

  //     let getImagePosition = (img: HTMLImageElement) => {
  //       try {
  //         let { top, left, width, height } = img.getBoundingClientRect()
  //         return {
  //           img,
  //           top,
  //           left,
  //           width,
  //           height,
  //         }
  //       } catch (e) {
  //         return null
  //       }
  //     }

  //     let updateImages = () => {
  //       arr = arr.map(getImagePosition)
  //       setImages(arr.filter((i) => i !== null))
  //       updateImagePositions(0)
  //       console.log(arr)
  //     }

  //     imagesloaded(scrollContainer.current, updateImages)

  //     window.addEventListener('resize', updateImages)
  //     return () => window.removeEventListener('resize', updateImages)
  //   }
  // }, [router.route])

  // useEffect(() => {
  //   scrollY.onChange((y) => {
  //     updateImagePositions(y)
  //   })
  // }, [scrollY])

  return (
    <>
      {/* <WebGL>
        {images?.map(
          ({ img, width, height, top, left }, index, arr) => {
            return (
              <mesh
                ref={(e) => (imageRefs.current[index] = e)}
                key={img.src + index}
              >
                <planeBufferGeometry args={[width, height, 1]} />
                <meshBasicMaterial
                  color={(0xffffff * index) / arr.length}
                />
              </mesh>
            )
          }
        )}
      </WebGL> */}
      <div ref={scrollContainer}>
        <Wrapper>
          <Component {...pageProps} />
        </Wrapper>
      </div>
    </>
  )
}
