# zachkrall/website

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c41d6cba299443beb4c8787bc9a8c20b)](https://www.codacy.com/manual/zachkrall/zachkrall.com?utm_source=gitlab.com&utm_medium=referral&utm_content=zachkrall/zachkrall.com&utm_campaign=Badge_Grade)

## about

Hello! This is my personal website.

## tools

- TypeScript
- Next.js
- Framer Motion
- Styled Components
- Three.js
